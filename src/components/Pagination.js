// Pagination component
angular.module('BoscApp')
.component('pagination', {
    templateUrl: '/html/pagination.html',
    controller: ['$scope', 'Content', function ($scope, Content) {
        var limit, total;
        $scope.init = function() {
            var p = Content.getPagination();
            limit = p.itemsPerPage;
            total = p.total;
            this.$ctrl.page = p.currentPage;
            if(limit >= total)
            {
                $scope.totalPages = 1;
                return false;
            }
            $scope.totalPages = Math.ceil(total/limit);
        };
        
        $scope.$on('build.pagination', function(n){
            $scope.init();
        });
        
        $scope.changePage = function(type){
            var page = parseInt(this.$ctrl.page);
            if (isNaN(page))
            {
                page = 1;
            }
            if (type != undefined)
            {
                page += (type == 'next' ? 1 : -1);
            }
            if (page > $scope.totalPages)
            {
                page = $scope.totalPages;
            }
            if (page < 1)
            {
                page = 1;
            }
            this.$ctrl.page = page;
            Content.setPagination(page, limit);
        };
        
    }]
});