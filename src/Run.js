angular.module('BoscApp', ['ngMaterial'])
.run(['$rootScope', '$location', '$http', '$mdDialog', '$timeout',
    function($rootScope, $location, $http, $mdDialog, $timeout, User){
    $rootScope.loading = true;
    $rootScope.host = $location.host();
    $rootScope.protocol = $location.protocol() + '://';
    $rootScope.search = $location.search();
    $rootScope.itemStates = {
        todo: 'To do',
        tocomplete: 'To complete',
        done: 'Done'
    };

    $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $http.defaults.transformResponse.push(function(response, callback, status){
        if (status === 401)
        {
            $rootScope.showAlert('Error', response.message || 'Unauthorized request');
            setTimeout(function () {
                location.href = '/';
            }, 1000);
            return false;
        } else if (status !== 200) {
            $rootScope.showAlert('Error', response.message || 'Internal server error: ' + status);
        }
        return response;
    });

    //custom alert
    $rootScope.showAlert = function (text, title) {
        $mdDialog.show(
            $mdDialog
                .alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title(title || 'Error')
                .textContent(text)
                .ok('Close')
        );
        return false;
    };
    
    //get domain from url
    $rootScope.getDomain = function(url) {
        var domain;
        if (url.indexOf("://") > -1) {
            domain = url.split('/')[2];
        }
        else {
            domain = url.split('/')[0];
        }

        domain = domain.split(':')[0];
        return domain;
    };

    //for async queries
    var stepCounter = 0,
        maxSteps = 2;
    $rootScope.$on('setMaxSteps', function (e, data) {
        maxSteps = parseInt(data, 10);
    });

    $rootScope.$on('stepCompleted', function () {
        stepCounter++;
        if (stepCounter === maxSteps)
        {
            $timeout(function () {
                $rootScope.loading = false;
            }, 300);
        }
    });
}]);