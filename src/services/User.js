/*
 * User service. Get user info from server.
 * 
 * @function me() returns user info
 * @function isManager() returns if user is a manager
 * @function isWriter() returns if user is a writer (for testing)
 */
angular.module('BoscApp')
.factory('User', ['$http', '$rootScope', function($http, $rootScope){
    var user = {};
    //get user from server
    $http.get('/data/user.json').then(
        function(response){
            if(response.data.success)
            {
                user = response.data.user;
            }
            else
            {
                $rootScope.showAlert('Error', (response.data.message || 'Error during user request'));
            }
            $rootScope.$emit('stepCompleted');
        },
        function(error){
            $rootScope.showAlert('Error', 'Error during user request');
            $rootScope.$emit('stepCompleted');
        }
    );
    
    return {
        me: function() {
            return user;
        },
        isManager: function() {
            return !!$rootScope.search.manager;
            //return user.role === 'manager';
        },
        isWriter: function() {
            return !!$rootScope.search.writer;
        }
    };
}]);
