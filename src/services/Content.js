/*
 * Content service
 * 
 * @function _getItems() returns filtered array of items
 * @function getItems() returns spliced array of items
 * @function getItemsCount() returns total count of filtered items
 * @function getItem() returns an item object by process.id
 * @function getIndexById() search process.id in items list and returns an index of item in array
 * @function getPagination() returns data for pagination
 * @function updateView() update html view (trigger 2 events: list.update and build.pagination)
 * @function setPagination() set pagination data
 * @function setItem() create or update item
 * @function setSearch() set search string
 * @function removeItem() remove item from list
 */
angular.module('BoscApp')
.factory('Content', ['$http', '$rootScope', '$filter', 'User', function($http, $rootScope, $filter, User){
    var items = [];
    var itemsPerPage = 10;
    var currentPage = 1;
    var searchStr = '';
    
    //get items from server
    $http.get('/data/reviews-data.json').then(
        function(response){
            if(response.data && response.data.length > 0)
            {
                items = response.data;
            }
            else
            {
                $rootScope.showAlert('Error', (response.data.message || 'Error during sources request'));
            }
            $rootScope.$emit('stepCompleted');
        },
        function(){
            $rootScope.showAlert('Error', 'Error during user request');
            $rootScope.$emit('stepCompleted');
        }
    );
    
    return {
        //filter items 
        _getItems: function() {
            if(searchStr.length === 0 )
            {
                return items;
            }
            return $filter('filter')(items, {$: searchStr});
        },
        //get list of items
        getItems: function(page) {
            if(page !== undefined)
            {
                currentPage = page;
            }
            return this._getItems().slice((currentPage-1) * itemsPerPage, currentPage * itemsPerPage);
        },
        //get total amount of filtered items
        getItemsCount: function() {
            return this._getItems().length;
        },
        //get item to edit
        getItem: function(id) {
            var item = {};
            if(id === undefined)
            {
                item = {
                    process: {
                        state: 'tocomplete'
                    },
                    review: {
                        tags: [],
                        rating: 1
                    },
                    place: {
                        address: {}
                    },
                    sources: []
                };
            }
            else
            {
                var index = this.getIndexById(id);
                item = items[index];
            }
            return item;
        },
        //get index of item by id
        getIndexById: function(id) {
            for(var i in items)
            {
                if(items[i].process.id === id)
                {
                    return i;
                }
            }
            return false;
        },
        //get pagination info
        getPagination: function() {
            return {
                currentPage: currentPage,
                itemsPerPage: itemsPerPage,
                total: this.getItemsCount()
            };
        },
        //update html view
        updateView: function() {
            $rootScope.$broadcast('build.pagination');
            $rootScope.$broadcast('list.update');
        },
        //set pagination
        setPagination: function(page, limit) {
            currentPage = parseInt(page);
            itemsPerPage = parseInt(limit);
            this.updateView();
        },
        //save item
        setItem: function(data, id) {
            if(!User.isManager())
            {
                return false;
            }
            
            if(data.sources.length === 0)
            {
                alert('You should add at least one source');
                return false;
            }
            
            if(id === undefined)
            {
                data.process.added = new Date().getTime();
                data.process.id = (items.length !== 0 ? (items[items.length-1].process.id) : 0) + 1;
                items.push(data);
            }
            else
            {
                var index = this.getIndexById(id);
                items[index] = data;
            }
            $rootScope.$broadcast('list.update');
            return true;
        },
        //set search query
        setSearch: function(search){
            searchStr = search.toLowerCase();
            this.updateView();
            return search;
        },
        //remove item
        removeItem: function(id) {
            var index = this.getIndexById(id);
            items.splice(index, 1);
            this.updateView();
            return true;
        }
    };
}]);