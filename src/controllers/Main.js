//main controller
angular.module('BoscApp')
.controller('Main', ['$scope', '$mdDialog', 'User', 'Content', function($scope, $mdDialog, User, Content){
    //format numbers
    function dubleChar(num) {
        return num < 10 ? '0' + num : num;
    }
    //wait while all requests will be completed, may be better via events?
    $scope.init = false;
    $scope.$watch('loading', function(n){
        if(n === false)
        {
            $scope.isManager = User.isManager();
            Content.updateView();
            $scope.init = true;
        }
    });
    
    //date format
    $scope.getDate = function(date){
        var d = new Date(date);
        return dubleChar(d.getMonth()+1) + '/' + dubleChar(d.getDate()) + '/' + d.getFullYear() + ' ' + dubleChar(d.getHours()) + ':' + dubleChar(d.getMinutes());
    };
    
    //draw rate
    $scope.getRateWidth = function(rate) {
        return {width: Math.floor(rate/5*100) + '%'};
    };
    
    //stopPropagation for events
    $scope.stopPropagation = function(e) {
        e.stopPropagation();
        e.stopImmediatePropagation();
    };
    
    //remove item from list
    $scope.removeItem = function(e, id) {
        $scope.stopPropagation(e);
        Content.removeItem(id);
        $scope.items = Content.getItems();
    };
    
    //set search
    $scope.search = '';
    $scope.searchS = function() {
        Content.setSearch($scope.search);
    };
    
    //listen update event
    $scope.$on('list.update', function(){
        $scope.items = Content.getItems();
    });
    
    //show the dialog
    $scope.openDialog = function(id) {
        var template = '/html/dialog.tmpl.html';
        if(User.isManager() && (!id || !User.isWriter()))
        {
            template = '/html/m-dialog.tmpl.html';
        }
        $mdDialog.show({
            controller: ['$scope', '$mdDialog', DialogController.bind(null, id)],
            templateUrl: template,
            clickOutsideToClose: true
        });
    };
    
    //separated controller for dialogs
    function DialogController(id, $scope, $mdDialog) {
        var checkUrlRegex = new RegExp(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi);
        
        $scope.closeDialog = function() {
            $mdDialog.hide();
        };
        
        $scope.saveItem = function() {
            if(Content.setItem($scope.item, id))
            {
                $mdDialog.hide();
            }
        };
        
        $scope.item = Content.getItem(id);
        if(User.isManager())
        {
            //add handler for sources list
            $scope.newSource = function(url) {
                if(!url.match(checkUrlRegex))
                {
                    $scope.showError = true;
                    return null;
                }
                $scope.showError = false;
                return {
                    url: url,
                    rating: $scope.item.review.rating,
                    tags: []
                };
            };
        }
        $scope.states = $scope.$root.itemStates;
        $scope.getDomain = $scope.$root.getDomain;
    };
    
}]);