var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    S3 = require('gulp-s3-upload'),
    fs = require('fs');
    
gulp.task('check', function(){
    gulp.src(['src/Run.js', 'src/**/*.js'])
        .pipe(concat('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./js'));
});

gulp.task('watch', function(){
    gulp.watch(['src/Run.js', 'src/**/*.js'], function () {
        gulp.run('check');
    });
});

gulp.task('upload', function(){
    var home = process.env[(process.platform === 'win32') ? 'USERPROFILE' : 'HOME'];
    var config = JSON.parse(fs.readFileSync(home + '/.aws/credentials.json'));
    config.region = 'eu-central-1';
    var s3 = S3(config);
    gulp.src(['!node_modules/**/*', '!nbproject/**/*', '!gulpfile.js', '!package.json', '!src/**/*', './**/*'])
        .pipe(s3({
            Bucket: 'angular-dev.eugene-web.ru', 
            ACL:    'public-read'       //  Needs to be user-defined
        }))
    ;
});
